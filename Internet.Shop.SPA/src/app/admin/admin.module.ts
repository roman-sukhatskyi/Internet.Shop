import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntryPageComponent } from './entry-page/entry-page.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [EntryPageComponent]
})
export class AdminModule { }
