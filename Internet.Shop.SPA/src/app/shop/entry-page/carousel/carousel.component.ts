import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  images = [
    'https://images.ua.prom.st/1098702379__dsc8715.jpg', 
    'https://images.ua.prom.st/1098702378__dsc8597.jpg', 
    'https://images.ua.prom.st/1098702380__dsc8867.jpg',
    'https://images.ua.prom.st/1098702381__dsc8889.jpg'
  ]
  constructor() {  }

  ngOnInit() {
  }

}
