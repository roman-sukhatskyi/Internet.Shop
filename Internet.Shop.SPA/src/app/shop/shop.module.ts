import { SharedModule } from './../shared/shared.module';
import { ShopRoutingModule } from './shop-routing.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntryPageComponent } from './entry-page/entry-page.component';
import { AuthPageComponent } from './auth-page/auth-page.component';
import { HttpClientModule } from '@angular/common/http';
import { CategoryCardComponent } from './entry-page/category-card/category-card.component';
import { CarouselComponent } from './entry-page/carousel/carousel.component';
import { CategoriesListComponent } from './entry-page/categories-list/categories-list.component';
import { CategoriesFilterComponent } from './entry-page/categories-filter/categories-filter.component';

import { UICarouselModule } from 'ui-carousel';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ShopRoutingModule,
    SharedModule,
    UICarouselModule
  ],
  declarations: [
    EntryPageComponent,
    AuthPageComponent,
    CategoriesListComponent,
    CategoryCardComponent,
    CarouselComponent,
    CategoriesFilterComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShopModule {}
