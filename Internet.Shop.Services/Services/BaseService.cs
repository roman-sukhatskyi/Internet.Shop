﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Internet.Shop.DataAccess.Interfaces;
using Internet.Shop.Services.Intefaces;

namespace Internet.Shop.Services.Services
{
    public abstract class BaseService<TEntity, TRepository> : IService<TEntity> where TEntity : class, new() where TRepository : IRepository<TEntity>
    {
        protected readonly IUnitOfWork Uow;
        protected readonly TRepository Repository;

        public BaseService(IUnitOfWork uow, TRepository repository)
        {
            Uow = uow;
            Repository = repository;
        }


        public TEntity Find(Guid? id)
        {
            return Repository.Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return Repository.GetAll();
        }

        public void Add(TEntity item)
        {
            Repository.Add(item);
            Uow.SaveChanges();
        }

        public void Add(IEnumerable<TEntity> items)
        {
            Repository.Add(items);
            Uow.SaveChanges();
        }

        public void Update(TEntity item)
        {
            Repository.Update(item);
            Uow.SaveChanges();
        }

        public void Update(IEnumerable<TEntity> items)
        {
            var enumerator = items.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Repository.Update(enumerator.Current);
            }
            Uow.SaveChanges();
        }

        public void Delete(Guid? id)
        {
            Repository.Delete(id);
            Uow.SaveChanges();
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> expression)
        {
            return Repository.Where(expression);
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> expression)
        {
            return Repository.SingleOrDefault(expression);
        }
    }
}
