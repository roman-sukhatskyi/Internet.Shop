﻿using System;
using System.Collections.Generic;
using Internet.Shop.DataAccess.Interfaces;
using Internet.Shop.DataAccess.Models;
using Internet.Shop.Services.Intefaces;
using Microsoft.AspNetCore.Mvc;

namespace Internet.Shop.Services.Services
{
    public class ProductCategoryService : BaseService<ProductCategory, IRepository<ProductCategory>>, IProductCategoryService
    {
        public ProductCategoryService(IUnitOfWork uow, IRepository<ProductCategory> repository) : base(uow, repository)
        {
            
        }

        [HttpPost("create")]
        public void Create(ProductCategory productCategory, Guid imageId)
        {
            var category = new ProductCategory
            {
                Id = productCategory.Id,
                CreatedOn = productCategory.CreatedOn,
                ModifiedOn = productCategory.ModifiedOn,
                Name = productCategory.Name,
                Code = productCategory.Code,
                ImageId = imageId
            };
            Repository.Add(category);
            Uow.SaveChanges();
        }

        [HttpGet("getAll")]
        public IEnumerable<ProductCategory> GetCategories()
        {
            var categories = Repository.GetAll();
            return categories;
        }

    }
}
