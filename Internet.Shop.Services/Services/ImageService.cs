﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Internet.Shop.DataAccess.Interfaces;
using Internet.Shop.DataAccess.Models;
using Internet.Shop.Services.Intefaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Internet.Shop.Services.Services
{
    public class ImageService : BaseService<Image, IRepository<Image>>, IImageService
    {
        public ImageService(IUnitOfWork uow, IRepository<Image> repository) : base(uow, repository)
        {
        
        }

        public void Upload(Image image)
        {   
            Repository.Add(image);
            Uow.SaveChanges();
        }

    }
}
