using System.ComponentModel.DataAnnotations;

namespace Internet.Shop.Services.DTOs
{
    public class UserForLoginDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}