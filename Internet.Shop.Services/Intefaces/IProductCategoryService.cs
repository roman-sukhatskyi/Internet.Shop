﻿using System;
using System.Collections.Generic;
using Internet.Shop.DataAccess.Models;

namespace Internet.Shop.Services.Intefaces
{
    public interface IProductCategoryService : IService<ProductCategory>
    {
        void Create(ProductCategory productCategory, Guid imageId);
        IEnumerable<ProductCategory> GetCategories();
    }
}
