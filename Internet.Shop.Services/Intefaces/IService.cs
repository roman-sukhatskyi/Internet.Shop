﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Internet.Shop.Services.Intefaces
{
    public interface IService<TEntity> where TEntity : class, new()
    {
        TEntity Find(Guid? id);
        IQueryable<TEntity> GetAll();
        void Add(TEntity item);
        void Add(IEnumerable<TEntity> items);
        void Update(TEntity item);
        void Update(IEnumerable<TEntity> items);
        void Delete(Guid? id);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> expression);
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> expression);
    }
}
