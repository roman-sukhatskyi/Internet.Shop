﻿using Internet.Shop.DataAccess.Models;

namespace Internet.Shop.Services.Intefaces
{
    public interface IImageService : IService<Image>
    {
        void Upload(Image files);
    }
}
