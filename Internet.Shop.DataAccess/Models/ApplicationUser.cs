﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Internet.Shop.DataAccess.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
