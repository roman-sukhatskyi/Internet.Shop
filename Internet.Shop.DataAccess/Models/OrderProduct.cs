﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet.Shop.DataAccess.Models
{
    public class OrderProduct : BaseEntity
    {
        public int? Quantity { get; set; }

        public decimal? Amount { get; set; }
    }
}
