﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet.Shop.DataAccess.Models
{
    public class Product : BaseEntity
    {
        public int? Code { get; set; }

        public decimal? Price { get; set; }

        public string Description { get; set; }
    }
}
