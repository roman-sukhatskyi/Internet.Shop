﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Internet.Shop.DataAccess.Models
{
    public class ProductCategory : BaseEntity
    {
        public int Code { get; set; }


        [ForeignKey("Image")]
        public Guid ImageId { get; set; }
        public virtual Image Image { get; set; }
    }
}
