﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet.Shop.DataAccess.Models
{
    public class Order : BaseEntity
    {
        public string Number { get; set; }
        public decimal Amount { get; set; }
    }
}
