using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Internet.Shop.Core.Attributes;

namespace Internet.Shop.DataAccess.Models
{
    public class BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [DefaultDateTimeValue("Now")]
        public DateTime? CreatedOn { get; set; }

        [DefaultDateTimeValue("Now")]
        public DateTime? ModifiedOn { get; set; }

        [Required]
        public string Name { get; set; }
    }
}