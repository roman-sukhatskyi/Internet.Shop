﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Internet.Shop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Internet.Shop.DataAccess.Implementation
{
    public class BaseRepository<T> : IRepository<T> where T : class 
    {
        protected readonly DbSet<T> DbSet;
        protected readonly ApplicationDbContext DbContext;

        public BaseRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = dbContext.Set<T>();
        }


        public T Find(Guid? id)
        {
            return DbSet.Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public void Add(T item)
        {
            DbSet.Add(item);
        }

        public void Add(IEnumerable<T> items)
        {
            DbSet.AddRange(items);
        }

        public void Update(T item)
        {
            DbContext.Entry(item).State = EntityState.Modified;
        }

        public void Delete(Guid? id)
        {
            var entity = DbSet.Find(id);
            DbSet.Remove(entity);
        }

        public T SingleOrDefault(Expression<Func<T, bool>> expression)
        {
            return DbSet.SingleOrDefault(expression);
        }

        public virtual IQueryable<T> Where(Expression<Func<T, bool>> expression)
        {
            return DbSet.Where(expression);
        }
    }
}
