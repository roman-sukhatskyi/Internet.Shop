using System;
using Internet.Shop.DataAccess.Implementation;

namespace Internet.Shop.DataAccess.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationDbContext Context { get; }
        void SaveChanges();
    }
}