﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Internet.Shop.DataAccess.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        #region Syncronous
        TEntity Find(Guid? id);
        IQueryable<TEntity> GetAll();
        void Add(TEntity item);
        void Add(IEnumerable<TEntity> items);
        void Update(TEntity item);
        void Delete(Guid? id);
        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> expression);
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> expression);
        #endregion
    }
}
