﻿using System;
using System.IO;
using Internet.Shop.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using ImageInfo = System.Drawing.Image;

namespace Internet.Shop.DataAccess.Helpers
{
    public static class ImageResolver
    {
        public static Image GetImageData(IFormFile file)
        {
            IFormFile uploadedImage = file;
            MemoryStream ms = new MemoryStream();

            uploadedImage.OpenReadStream().CopyTo(ms);

            ImageInfo imageInfo = ImageInfo.FromStream(ms);

            var image = new Image
            {
                Id = Guid.NewGuid(),
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                Name = uploadedImage.FileName,
                Data = ms.ToArray(),
                Width = imageInfo.Width,
                Length = uploadedImage.Length,
                Height = imageInfo.Height,
                ContentType = uploadedImage.ContentType
            };

            return image;
        }
    }
}
