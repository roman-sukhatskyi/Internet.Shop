﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Internet.Shop.DataAccess.Helpers;
using Internet.Shop.DataAccess.Models;
using Internet.Shop.Services.Intefaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Internet.Shop.API.Controllers
{
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        private readonly IImageService _imageService;

        public ImageController(IImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpPost("upload")]
        public IActionResult UploadImage(IList<IFormFile> files)
        {
            IFormFile uploadedImage = files.FirstOrDefault();

            if (uploadedImage == null)
                return BadRequest();

            var image = ImageResolver.GetImageData(uploadedImage);

            _imageService.Upload(image);

            return Ok();
        }

        [HttpGet("view")]
        public FileStreamResult ViewImage(Guid id)
        {
            Image image = _imageService.SingleOrDefault(m => m.Id == id);
            MemoryStream ms = new MemoryStream(image.Data);

            return new FileStreamResult(ms, image.ContentType);
        }
    }
}
