﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Internet.Shop.DataAccess.Helpers;
using Internet.Shop.DataAccess.Models;
using Internet.Shop.Services.Intefaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Internet.Shop.API.Controllers
{
    [Route("api/[controller]")]
    public class ProductCategoryController : Controller
    {
        private readonly IProductCategoryService _productCategoryService;
        private readonly IImageService _imageService;

        public ProductCategoryController(IProductCategoryService productCategoryService, 
            IImageService imageService)
        {
            _productCategoryService = productCategoryService;
            _imageService = imageService;
        }

        [HttpGet("getAll")]
        [AllowAnonymous]
        public IEnumerable<ProductCategory> Get()
        {
            return _productCategoryService.GetCategories();
        }

        [HttpPost("createCategory")]
        public IActionResult Add([FromBody] ProductCategory request, IList<IFormFile> files)
        {
            var category = _productCategoryService.SingleOrDefault(x => x.Code == request.Code);
            IFormFile uploadedImage = files.FirstOrDefault();

            if (category != null && uploadedImage != null)
            {
                var image = ImageResolver.GetImageData(uploadedImage);

                _imageService.Upload(image);

                _productCategoryService.Create(request, image.Id);
            }
            return Ok();
        }
        
        [HttpGet("delete")]
        public ActionResult Delete(Guid? id)
        {
            var category = _productCategoryService.SingleOrDefault(x => x.Id == id);
            if (category == null)
                return BadRequest();

            _productCategoryService.Delete(id);

            return Ok();
        }
    }
}
